//
//  CallOutAPI.swift
//  CallOut
//
//  Created by Topi Penttilä on 21/04/16.
//  Copyright © 2016 FeedBack TK. All rights reserved.
//

import Foundation

let callOutApiSingleton = CallOutAPI()

class CallOutAPI: NSObject {
    private override init() {
        super.init()
        get("users")

    }
    var workspaces: Array<Workspace> = []
    var users: Array<User> = []
    var beacons: Array<Beacon> = []
    var subscriptions: Array<Subscription> = []
    var statuses: Array<Status> = []
    
    var userData : NSArray = []
    var urlString = "http://julius.niiniranta.net/callout/"
    let defaults = NSUserDefaults.standardUserDefaults()
    
    func get(type: String) {
        
        var dbUrl: NSURL
        
        let session = NSURLSession.sharedSession()
        if type == "users" {
            dbUrl = NSURL(string: urlString + "users.json")!
        }
        else if type == "beacons" {
            dbUrl = NSURL(string: urlString + "beacons.json")!
        } else if type == "workspaces" {
            dbUrl = NSURL(string: urlString + "workspaces.json")!
        } else if type == "subscriptions" {
            dbUrl = NSURL(string: urlString + "subscriptions.json")!
        } else if type == "statuses" {
            dbUrl = NSURL(string: urlString + "statuses.json")!
        }
        else {
            print("wrong type")
            dbUrl = NSURL(string: urlString)!
        }
        
        let task = session.dataTaskWithURL(dbUrl) {
            (data, response, error) -> Void in
            
            if error != nil {
                print(error!.localizedDescription)
            } else {
                
                do {
                    self.userData = try NSJSONSerialization.JSONObjectWithData(data!, options: .MutableContainers) as! NSArray
                    
                } catch let error as NSError {
                    print(error.localizedDescription)
                }
                if type == "users" {
                    self.users.removeAll()
                    for user in self.userData {
                        let user = User(data: user as! NSDictionary)
                        self.users.append(user)
                    }
                }
                else if type == "beacons" {
                    self.beacons.removeAll()
                    for beacon in self.userData {
                        let beacon = Beacon(data: beacon as! NSDictionary)
                        self.beacons.append(beacon)
                    }
                }
                else if type == "workspaces" {
                    self.workspaces.removeAll()
                    for workspace in self.userData {
                        let workspace = Workspace(data: workspace as! NSDictionary)
                        self.workspaces.append(workspace)
                    }
                }
                else if type == "subscriptions" {
                    self.subscriptions.removeAll()
                    for subscription in self.userData {
                        let subscription = Subscription(data: subscription as! NSDictionary)
                        self.subscriptions.append(subscription)
                    }
                }
                else if type == "statuses" {
                    self.statuses.removeAll()
                    for status in self.userData {
                     let status = Status(data: status as! NSDictionary)
                     self.statuses.append(status)
                     }
                }
            }
        }
        task.resume()
    }
    
    func postUser (firstName: String, lastName: String, emailAddress: String/*, subscriptions: Array<Subscription>*/){
        
        let para:NSMutableDictionary = NSMutableDictionary()
        para.setValue(firstName, forKey: "first_name")
        para.setValue(lastName, forKey: "last_name")
        para.setValue(emailAddress, forKey: "email_address")
        /*para.setValue(subscriptions, forKey: "subscriptions")*/
        let jsonData = try! NSJSONSerialization.dataWithJSONObject(para, options: NSJSONWritingOptions())
        let jsonString = NSString(data: jsonData, encoding: NSUTF8StringEncoding) as! String
        let session = NSURLSession.sharedSession()
        let request = NSMutableURLRequest()
        request.HTTPBody = jsonString.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: true)
        request.HTTPMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.URL = NSURL(string: urlString + "users.json")
        
        
        let task = session.dataTaskWithRequest(request, completionHandler: {(data, response, error) -> Void in
            
            let parseOperation = NSBlockOperation(block: {
                guard error == nil else {
                    print(error!.localizedDescription)
                    return
                }
                do {
                    let jsonObject = try NSJSONSerialization.JSONObjectWithData(data!, options: .AllowFragments)
                    if let dictionary = jsonObject as? [String: AnyObject] {
                        guard let id = dictionary["id"] as? Int else {
                            print("No id provided")
                            print("response" + "\(response)")
                            print("error" + "\(error)")
                            print(jsonString)
                            return
                        }
                        print(jsonString)
                        print("response" + "\(response)")
                        print("User created with id " + String(id))
                        
                        self.get("users")
                        self.get("workspaces")
                        self.get("beacons")
                        self.get("subscriptions")
                        self.get("statuses")
                        self.defaults.setInteger(id, forKey: "userId")
                        
                    }
                } catch {
                    print("JSONParseError: \(error)")
                    return
                }
            })
            let queue = NSOperationQueue()
            queue.addOperation(parseOperation)
        })
        task.resume()
    }
    
    func postWorkspace (name: String, blist: Array<Int>, creator: Int){
        
        let para:NSMutableDictionary = NSMutableDictionary()
        para.setValue(name, forKey: "name")
        //para.setValue("0000-00-00", forKey: "date_ending")
        para.setValue(blist, forKey: "beacons")
        //para.setValue("0000-00-00", forKey: "date_starting")
        para.setValue(creator, forKey: "creator")
        
        let jsonData = try! NSJSONSerialization.dataWithJSONObject(para, options: NSJSONWritingOptions())
        let jsonString = NSString(data: jsonData, encoding: NSUTF8StringEncoding) as! String
        print(jsonString)
        
        let session = NSURLSession.sharedSession()
        let request = NSMutableURLRequest()
        request.HTTPBody = jsonString.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: true)
        request.HTTPMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.URL = NSURL(string: urlString + "workspaces.json")
        
        
        let task = session.dataTaskWithRequest(request, completionHandler: {(data, response, error) -> Void in
            
            let parseOperation = NSBlockOperation(block: {
                guard error == nil else {
                    print(error!.localizedDescription)
                    return
                }
                do {
                    let jsonObject = try NSJSONSerialization.JSONObjectWithData(data!, options: .AllowFragments)
                    if let dictionary = jsonObject as? [String: AnyObject] {
                        guard let id = dictionary["id"] as? Int else {
                            print("No id provided")
                            print("response" + "\(response)")
                            print("error" + "\(error)")
                            print(jsonString)
                            return
                        }
                        print("Workspace created with id " + String(id))
                        
                        self.get("users")
                        self.get("workspaces")
                        self.get("beacons")
                        self.get("subscriptions")
                        self.get("statuses")
                        self.defaults.setObject(id, forKey: "workspaceId")
                        
                    }
                } catch {
                    print("JSONParseError: \(error)")
                    return
                }
            })
            let queue = NSOperationQueue()
            queue.addOperation(parseOperation)
        })
        task.resume()
    }
    
    func postSubscription (user: Int, workspace: Int, role: Int, feedback: String){
        let para:NSMutableDictionary = NSMutableDictionary()
        para.setValue(user, forKey: "user")
        para.setValue(workspace, forKey: "workspace")
        para.setValue(role, forKey: "role")
        para.setValue(feedback, forKey: "feedback")
        
        let jsonData = try! NSJSONSerialization.dataWithJSONObject(para, options: NSJSONWritingOptions())
        let jsonString = NSString(data: jsonData, encoding: NSUTF8StringEncoding) as! String
        print(jsonString)
        
        let session = NSURLSession.sharedSession()
        let request = NSMutableURLRequest()
        request.HTTPBody = jsonString.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: true)
        request.HTTPMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.URL = NSURL(string: urlString + "subscriptions.json")
        
        let task = session.dataTaskWithRequest(request, completionHandler: {(data, response, error) -> Void in
            
            let parseOperation = NSBlockOperation(block: {
                guard error == nil else {
                    print(error!.localizedDescription)
                    return
                }
                do {
                    let jsonObject = try NSJSONSerialization.JSONObjectWithData(data!, options: .AllowFragments)
                    if let dictionary = jsonObject as? [String: AnyObject] {
                        guard let id = dictionary["id"] as? Int else {
                            print("No id provided")
                            print("response" + "\(response)")
                            print("error" + "\(error)")
                            print(jsonString)
                            return
                        }
                        print("Subscription created with id " + String(id))
                        
                        self.defaults.setObject(id, forKey: "subscriptionId")
                        self.getAll()
                        
                    }
                } catch {
                    print("JSONParseError: \(error)")
                    return
                }
            })
            let queue = NSOperationQueue()
            queue.addOperation(parseOperation)
        })
        task.resume()
    }
    
    func postBeacon (major: String, minor: String, uuid: String){
        
        let para:NSMutableDictionary = NSMutableDictionary()
        para.setValue(major, forKey: "major")
        para.setValue(minor, forKey: "minor")
        para.setValue(uuid, forKey: "uuid")
        
        let jsonData = try! NSJSONSerialization.dataWithJSONObject(para, options: NSJSONWritingOptions())
        let jsonString = NSString(data: jsonData, encoding: NSUTF8StringEncoding) as! String
        print(jsonString)
        
        let session = NSURLSession.sharedSession()
        let request = NSMutableURLRequest()
        request.HTTPBody = jsonString.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: true)
        request.HTTPMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.URL = NSURL(string: urlString + "beacons.json")
        
        
        let task = session.dataTaskWithRequest(request, completionHandler: {(data, response, error) -> Void in
            
            let parseOperation = NSBlockOperation(block: {
                guard error == nil else {
                    print(error!.localizedDescription)
                    return
                }
                do {
                    let jsonObject = try NSJSONSerialization.JSONObjectWithData(data!, options: .AllowFragments)
                    if let dictionary = jsonObject as? [String: AnyObject] {
                        guard let id = dictionary["id"] as? Int else {
                            print("No id provided")
                            print("response" + "\(response)")
                            print("error" + "\(error)")
                            print(jsonString)
                            return
                        }
                        print("Beacon created with id " + String(id))
                        
                        self.defaults.setInteger(id, forKey: "tempBeaconId")
                        self.getAll()
                        
                        
                    }
                } catch {
                    print("JSONParseError: \(error)")
                    return
                }
            })
            let queue = NSOperationQueue()
            queue.addOperation(parseOperation)
        })
        task.resume()
    }
    
    func postStatus (state: String, subscription: Int){
        
        let para:NSMutableDictionary = NSMutableDictionary()
        para.setValue(state, forKey: "state")
        para.setValue(subscription, forKey: "subscription")
        
        let jsonData = try! NSJSONSerialization.dataWithJSONObject(para, options: NSJSONWritingOptions())
        let jsonString = NSString(data: jsonData, encoding: NSUTF8StringEncoding) as! String
        print(jsonString)
        
        let session = NSURLSession.sharedSession()
        let request = NSMutableURLRequest()
        request.HTTPBody = jsonString.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: true)
        request.HTTPMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.URL = NSURL(string: urlString + "statuses.json")
        
        
        let task = session.dataTaskWithRequest(request, completionHandler: {(data, response, error) -> Void in
            
            let parseOperation = NSBlockOperation(block: {
                guard error == nil else {
                    print(error!.localizedDescription)
                    return
                }
                do {
                    let jsonObject = try NSJSONSerialization.JSONObjectWithData(data!, options: .AllowFragments)
                    if let dictionary = jsonObject as? [String: AnyObject] {
                        guard let id = dictionary["id"] as? Int else {
                            print("No id provided")
                            print("response" + "\(response)")
                            print("error" + "\(error)")
                            print(jsonString)
                            return
                        }
                        print("Status created with id " + String(id))
                        
                        self.defaults.setObject(id, forKey: "lasStatusId")
                        self.getAll()
                    
                        
                        
                    }
                } catch {
                    print("JSONParseError: \(error)")
                    return
                }
            })
            let queue = NSOperationQueue()
            queue.addOperation(parseOperation)
        })
        task.resume()
    }
    
    func deleteWorkspace (id: Int){
        
        let para:NSMutableDictionary = NSMutableDictionary()
        para.setValue(id, forKey: "id")
        
        //let jsonData = try! NSJSONSerialization.dataWithJSONObject(para, options: NSJSONWritingOptions())
        //let jsonString = NSString(data: jsonData, encoding: NSUTF8StringEncoding) as! String
        //print(jsonString)
        
        let session = NSURLSession.sharedSession()
        let request = NSMutableURLRequest()
        //request.HTTPBody = jsonString.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: true)
        request.HTTPMethod = "DELETE"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.URL = NSURL(string: urlString + "workspaces/" + "\(id)" + ".json")
        
        
        let task = session.dataTaskWithRequest(request, completionHandler: {(data, response, error) -> Void in
            
            let parseOperation = NSBlockOperation(block: {
                guard error == nil else {
                    print(error!.localizedDescription)
                    return
                }
                
                print("response" + "\(response)")
                print("error" + "\(error)")
                print("Workspace deleted with id " + String(id))
                self.getAll()
                

                
            })
            let queue = NSOperationQueue()
            queue.addOperation(parseOperation)
        })
        task.resume()
    }
    
    func deleteAccount (id: Int){
        
        let session = NSURLSession.sharedSession()
        let request = NSMutableURLRequest()
        //request.HTTPBody = jsonString.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: true)
        request.HTTPMethod = "DELETE"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.URL = NSURL(string: urlString + "users/" + "\(id)" + ".json")
        
        
        let task = session.dataTaskWithRequest(request, completionHandler: {(data, response, error) -> Void in
            
            let parseOperation = NSBlockOperation(block: {
                guard error == nil else {
                    print(error!.localizedDescription)
                    return
                }
                
                print("response" + "\(response)")
                print("error" + "\(error)")
                print("User Account deleted with id " + String(id))
                
                
                
                self.getAll()
                
            })
            let queue = NSOperationQueue()
            queue.addOperation(parseOperation)
        })
        task.resume()
    }
    
    func getAll() {
        self.get("users")
        self.get("workspaces")
        self.get("beacons")
        self.get("subscriptions")
        self.get("statuses")
    }
    
    
    
}
