//
//  BeaconDetector.swift
//  CallOut
//
//  Created by iosdev on 21.4.2016.
//  Copyright © 2016 FeedBack TK. All rights reserved.
//

import Foundation
import CoreLocation

let detectorSingleton = BeaconDetector()
protocol BeaconDetectorDelegate{
    func didFinishTask()
}

class BeaconDetector : NSObject, CLLocationManagerDelegate {
    private override init() {
        super.init()
    }
    
    let api = callOutApiSingleton
    var bDelegate: BeaconDetectorDelegate?
    let locationManager = CLLocationManager()
    let region = CLBeaconRegion(proximityUUID: NSUUID(UUIDString: "00000000-0000-0000-0000-000000000000")!, identifier: "Ibecon")
    let estimoteRegion = CLBeaconRegion(proximityUUID: NSUUID(UUIDString: "B9407F30-F5F8-466E-AFF9-25556B57FE6D")!, identifier: "Estimotes")
    let jokuRegion = CLBeaconRegion(proximityUUID: NSUUID(UUIDString: "DBB26A86-A7FD-45F7-AEEA-3A1BFAC8D6D9")!, identifier: "Kulmapöytä")
    var knownBeacons: Array<CLBeacon> = []
    var beaconNames: Array<String> = []
    var maxBeacons: Array<CLBeacon> = []
    var beaconObjects: Array<Beacon> = []
    
    func locationManager(manager: CLLocationManager, didRangeBeacons beaconsx: [CLBeacon], inRegion region: CLBeaconRegion){
        knownBeacons = beaconsx.filter{ $0.proximity != CLProximity.Unknown }
        for beacon in knownBeacons {
            if !maxBeacons.contains(beacon){
                maxBeacons.append(beacon)
            }
        }
        bDelegate?.didFinishTask()
    }
    
    func stopRangingBeacons(){
        locationManager.stopRangingBeaconsInRegion(region)
        locationManager.stopRangingBeaconsInRegion(estimoteRegion)
        locationManager.stopRangingBeaconsInRegion(jokuRegion)
    }
    
    func rangeBeacons(){
        locationManager.delegate = self
        if (CLLocationManager.authorizationStatus() != CLAuthorizationStatus.AuthorizedWhenInUse){
            locationManager.requestWhenInUseAuthorization()
        }
        locationManager.startRangingBeaconsInRegion(region)
        locationManager.startRangingBeaconsInRegion(estimoteRegion)
        locationManager.startRangingBeaconsInRegion(jokuRegion)
    }
    
    func beaconAmount() -> Int{
        print("!!!!!!!!!!!!!!!!!!! TÄN VERRAN PEKONIA " + String(knownBeacons.count) + " !!!!!!!!!!!")
        if knownBeacons.count > 0 {
            return knownBeacons.count+1
        }
        else{
            return 1
        }
    }

    
}
