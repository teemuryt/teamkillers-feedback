//
//  WorkspaceDetector.swift
//  CallOut
//
//  Created by iosdev on 26.4.2016.
//  Copyright © 2016 FeedBack TK. All rights reserved.
//

import Foundation

let workspaceManagerSingleton = WorkspaceManager()


class WorkspaceManager: NSObject, NetworkManagerDelegate{
    
    private override init() {
        super.init()
        nwm.delegate = self
    }
    let beaconDetector = detectorSingleton
    let api = callOutApiSingleton
    var myWorkspaces: Array<Workspace> = []
    var allAvaWorkspaces: Array<Workspace> = []
    var avaWorkspaces: Array<Workspace> = []
    var beacons: Array<Dictionary<String, AnyObject>> = []
    let nwm = NetworkManager()
    
    
    
    
    func didFailToCompleteRequest(errorDescription: String){
        
    }
    func didCompleteRequest(objectModel: NetworkManager.ObjectModel, receivedResponse: NSHTTPURLResponse, receivedDataObject: Dictionary<String, AnyObject>?, receivedDataArray: Array<Dictionary<String, AnyObject>>?){
        
        if let receivedArray = receivedDataArray {
            print("AdminDataManager.receivedObject")
            switch objectModel {
            case .Beacons:
                guard let receivedBeaconList = receivedArray as? Array<Dictionary<String, AnyObject>> else {
                    return
                }
                beacons = receivedBeaconList
                print(beacons)
                
            case .Workspaces:
                guard let receivedWorkspacesList = receivedArray as? Array<Dictionary<String, AnyObject>> else {
                    return
                }

            default:
                print("Wrong object model")
                
            }
            
        }
        
        
        
        
        
        /*func iterationVtwo{
         for subscription in api.subscriptions{
         if subscription.user.id == api.defaults.integerForKey("userId"){
         if !myWorkspaces.contains(subscription.workspace){
         myWorkspaces.append(subscription.workspace)
         }
         }
         else if subscription.user.id != api.defaults.integerForKey("userId"){
         
         }
         {
         
         }
         }
         }*/
        
        
        func iteration(){
            for workspace in api.workspaces{
                for beaconId in workspace.beacons{
                    for beacon in api.beacons{
                        if beaconId == beacon.id{
                            for beaconx in beaconDetector.maxBeacons{
                                let beaconMajor = String(beaconx.major)
                                let beaconMinor = String(beaconx.minor)
                                if beaconMajor == beacon.major && beaconMinor == beacon.minor{
                                    for subscription in api.subscriptions{
                                        if subscription.workspace.id == workspace.id && subscription.user.id == api.defaults.integerForKey("userId"){
                                            if !myWorkspaces.contains(workspace){
                                                myWorkspaces.append(workspace)
                                            }
                                        }
                                        else if subscription.workspace.id != workspace.id && subscription.user.id != api.defaults.integerForKey("userId"){
                                            if !allAvaWorkspaces.contains(workspace){
                                                allAvaWorkspaces.append(workspace)
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        
        
        func testiAvaWorkspaces(){
            if !beaconDetector.maxBeacons.isEmpty{
                if allAvaWorkspaces.isEmpty && myWorkspaces.isEmpty{
                    iteration()
                }
                else{
                    for workspacez in allAvaWorkspaces{
                        if !api.workspaces.contains(workspacez){
                            allAvaWorkspaces.removeAll()
                            break
                        }
                    }
                    for workspacex in myWorkspaces{
                        if !api.workspaces.contains(workspacex){
                            myWorkspaces.removeAll()
                            break
                        }
                    }
                    iteration()
                }
            }
        }
        
        
        
        
        
        
    }
}
