//
//  AccountTabBarController.swift
//  CallOut
//
//  Created by Topi Penttilä on 10/05/16.
//  Copyright © 2016 FeedBack TK. All rights reserved.
//

import UIKit

class AccountTabBarController: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()
        //Set username to tab bar
        if NSUserDefaults.standardUserDefaults().valueForKey("username") != nil {
        self.title = String(NSUserDefaults.standardUserDefaults().valueForKey("username")!)
        }
        
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillDisappear(animated: Bool) {
        
        // Do any additional setup after loading the view.
        
        
        /*
         // MARK: - Navigation
         
         // In a storyboard-based application, you will often want to do a little preparation before navigation
         override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
         // Get the new view controller using segue.destinationViewController.
         // Pass the selected object to the new view controller.
         }
         */
        
    }
    
    // MARK: Observers
    
    func observeUserDefaults() {
        // When UserDefaults change, this callback method is evoked. It verifies that userId has a value and then segues back to the User View.
        if (NSUserDefaults.standardUserDefaults().objectForKey("userId") != nil) {
            dispatch_async(dispatch_get_main_queue(), {
                self.navigationController?.popViewControllerAnimated(true)
            })
        }
    }

}
