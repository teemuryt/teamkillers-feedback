//
//  StatusTableViewController.swift
//  CallOut
//
//  Created by Topi Penttilä on 18/04/16.
//  Copyright © 2016 FeedBack TK. All rights reserved.
//

import UIKit
import ChameleonFramework
class StatusTableViewController: UITableViewController, UITextFieldDelegate {
    
    @IBOutlet weak var workspaceName: UILabel!
    let api = callOutApiSingleton
    let statusDetector = StatusManager.sharedInstance
    var workspaceNameText = "Current workspace"
    var feedback: String = ""
    var textlolField: UITextField?
    

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }
    override func viewWillAppear(animated: Bool) {
        api.getAll()
        for subscription in api.subscriptions{
            if api.defaults.integerForKey("userId") == subscription.user && api.defaults.integerForKey("workspaceId") == subscription.workspace{
                if subscription.role == 1{
                    self.navigationItem.rightBarButtonItem!.enabled = false
                }
                if subscription.role == 2{
                    self.navigationItem.rightBarButtonItem!.enabled = true
                }
            }
        }
        if api.defaults.valueForKey("workspaceName") != nil{
            print("workspaceName displayed")
        workspaceName.text = String(api.defaults.valueForKey("workspaceName")!)
        }
    }
    override func viewDidAppear(animated: Bool) {
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 2
    }
    override func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 0{
            return ""
        }
        else{
            return "Input feedback for this workspace"
        }
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        if section == 0 {
            return 3
        }
        else{
            return 1
        }
    }
    
    func textFieldDidBeginEditing(textField: UITextField) {
        textlolField = textField
    }
    
    func textFieldDidEndEditing(textlolField: UITextField) {
        feedback = textlolField.text!
        // TÄHÄN ALERTTI JOKA KYSYY HALUATKO VARMASTI LÄHETTÄÄ TÄMÄN FEEDBACKIN SUBSCRIPTIONIIN!!!!
        
        /* TÄSSÄ VALMIS METODI SITÄ VARTEN
         
         statusDetector.nwm.httpRequest(NetworkManager.ObjectModel.Subscriptions, requestMethod: NetworkManager.RequestMethod.PATCH, individual: NSUserDefaults.standardUserDefaults().integerForKey("subscriptionId"), bodyParams: [
         "feedback": feedback
         ]as Dictionary<String, AnyObject>)
 
 */
    }
    
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        // Configure the cell...
        if indexPath.section == 0{
            let cell = tableView.dequeueReusableCellWithIdentifier("statusTableViewCell", forIndexPath: indexPath) as! StatusTableViewCell
            if indexPath.row == 0 {
                cell.myLabel.text = "Following"
                cell.backgroundColor = UIColor.greenColor()
                cell.myLabel.textColor = UIColor.whiteColor()
            } else if indexPath.row == 1 {
                cell.myLabel.text = "Unsure"
                cell.backgroundColor = UIColor.yellowColor()
                cell.myLabel.textColor = UIColor.whiteColor()
            } else if indexPath.row == 2 {
                cell.myLabel.text = "Lost"
                cell.backgroundColor = UIColor.redColor()
                cell.myLabel.textColor = UIColor.whiteColor()
            }
            return cell
        }
        else{
            let celltext = tableView.dequeueReusableCellWithIdentifier("statusFeedback") as! StatusFeedbackTableViewCell
            celltext.statusFeedback.delegate = self
            celltext.sendFeedback.tag = indexPath.row
            celltext.sendFeedback.addTarget(self, action: #selector(StatusTableViewController.buttonClicked(_:)), forControlEvents: UIControlEvents.TouchUpInside)
            return celltext
        }
    }
    func buttonClicked(sender:UIButton) {
        
        statusDetector.networkManager.httpRequest(NetworkManager.RequestMethod.POST, objectModel: NetworkManager.ObjectModel.Subscriptions, individual: NSUserDefaults.standardUserDefaults().integerForKey("subscriptionId"), bodyParams: [
            "feedback": feedback
            ]as Dictionary<String, AnyObject>)
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        var subscription = 1
        let lastStatus = api.defaults.integerForKey("lastStatus")
        for s in api.subscriptions {
            if api.defaults.integerForKey("workspaceId") == s.workspace && api.defaults.integerForKey("userId") == s.user {
                subscription = s.id
            }
        }
        //Status doesn't post if already clicked the item
        if indexPath.row == 0 {
            if lastStatus != 0 {
                api.postStatus("F", subscription: subscription)
                api.defaults.setInteger(0, forKey: "lastStatus")
            }
        } else if indexPath.row == 1 {
            if lastStatus != 1 {
                api.postStatus("U", subscription: subscription)
                api.defaults.setInteger(1, forKey: "lastStatus")
            }
        } else if indexPath.row == 2 {
            if lastStatus != 2 {
                api.postStatus("R", subscription: subscription)
                api.defaults.setInteger(2, forKey: "lastStatus")
            }
        } else if indexPath.row == 3 {
            
        }
    }
    
    @IBAction func viewAdmin(sender: AnyObject) {
        performSegueWithIdentifier("Stats", sender:self)
    }
    
    /*
     // Override to support conditional editing of the table view.
     override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
     // Return false if you do not want the specified item to be editable.
     return true
     }
     */
    
    /*
     // Override to support editing the table view.
     override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
     if editingStyle == .Delete {
     // Delete the row from the data source
     tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
     } else if editingStyle == .Insert {
     // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
     }
     }
     */
    
    /*
     // Override to support rearranging the table view.
     override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {
     
     }
     */
    
    /*
     // Override to support conditional rearranging of the table view.
     override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
     // Return false if you do not want the item to be re-orderable.
     return true
     }
     */
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    
}
