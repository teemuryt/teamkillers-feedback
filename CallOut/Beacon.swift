//
//  Beacon.swift
//  CallOut
//
//  Created by iosdev on 26.4.2016.
//  Copyright © 2016 FeedBack TK. All rights reserved.
//

import Foundation

class Beacon{
    var id: Int!
    var major: String!
    var minor: String!
    var uuid: String!
    
    init(data : NSDictionary) {
        
        self.id = data["id"] as! Int
        self.major = getStringFromJSON(data, key:"major")
        self.minor = getStringFromJSON(data, key:"minor")
        self.uuid = getStringFromJSON(data, key:"uuid")
        
    }
    
    
    func getStringFromJSON(data: NSDictionary, key: String) -> String {
        
        if let info = data[key] as? String {
            return info
        }
        else{
            return "getStringFromJson did not werk"
        }
    }
    
}