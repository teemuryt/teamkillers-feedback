//
//  LoginManager.swift
//  CallOut
//
//  Created by Julius Niiniranta on 19/05/16.
//  Copyright © 2016 FeedBack TK. All rights reserved.
//

import UIKit

protocol AccountManagerDelegate: class {
    func didGetUserData(userData: NSDictionary)
    func didCreateNewUser(userData: NSDictionary)
    func didModifyUser(userData: NSDictionary)
    func didDeleteUser(userData: NSDictionary)
}

class AccountManager: NSObject, NetworkManagerDelegate {
    
    var delegate: AccountManagerDelegate?
    
    let networkManager = NetworkManager()
    static let sharedInstance = AccountManager()
    private override init() {
        super.init()
        networkManager.delegate = self
    }

    func didFailToCompleteRequest(errorDescription: String) {
        print("AccountManager didFailToCompleteRequest():", errorDescription)
    }
    
    func didCompleteRequest(objectModel: NetworkManager.ObjectModel, receivedResponse: NSHTTPURLResponse, receivedDataObject: Dictionary<String, AnyObject>?, receivedDataArray: Array<Dictionary<String, AnyObject>>?) {
        if objectModel == .Users {
            switch receivedResponse.statusCode {
            //case 200:
                //self.delegate?.didGetUserData(receivedData)
            //case 201:
                //self.delegate?.didCreateNewUser(receivedData)
            //case 204:
                //self.delegate?.didDeleteUser(receivedData)
            default:
                print("Unknown response status code.")
            }
        }
    }
}
