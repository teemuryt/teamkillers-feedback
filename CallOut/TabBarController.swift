//
//  TabBarController.swift
//  CallOut
//
//  Created by Topi Penttilä on 10/05/16.
//  Copyright © 2016 FeedBack TK. All rights reserved.
//

import UIKit

let tabBarControllerSingleton = TabBarController()

class TabBarController: UITabBarController, AccountManagerDelegate {
    
    var originalViewControllers: [UIViewController] = []
    let accountManager = AccountManager.sharedInstance
    
    override func viewDidLoad() {
        super.viewDidLoad()
        accountManager.delegate = self
    }
    override func viewDidAppear(animated: Bool) {
    }
    
    // Do any additional setup after loading the view.
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        originalViewControllers = viewControllers!
    }
    
    // MARK: Observers
    
    func didGetUserData(userData: NSDictionary) {
        print("TabBarController.didGetUserData()")
    }
    
    func didModifyUser(userData: NSDictionary) {
        print("TabBarController.didModifyUser()")
    }
    
    func didCreateNewUser(userData: NSDictionary) {
        print("TabBarController.didCreateNewUser()")
    }
    
    func didDeleteUser(userData: NSDictionary) {
        print("TabBarController.didDeleteUser()")
    }
    
        /*override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
            if segue.identifier == "showLogin"{
                print("showLogin seque called")
                let bottomBar = segue.destinationViewController as! AccountTableViewController
                bottomBar.hidesBottomBarWhenPushed = true
                bottomBar.navigationItem.hidesBackButton = true
            }
        }*/

            
            /*
             // MARK: - Navigation
             
             // In a storyboard-based application, you will often want to do a little preparation before navigation
             override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
             // Get the new view controller using segue.destinationViewController.
             // Pass the selected object to the new view controller.
             }
             */
    
}