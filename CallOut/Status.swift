//
//  Status.swift
//  CallOut
//
//  Created by iosdev on 27.4.2016.
//  Copyright © 2016 FeedBack TK. All rights reserved.
//

import Foundation

class Status: NSObject{
    var id: Int!
    var state: String!
    var subscription: Int!
    
    init(data : NSDictionary) {
        super.init()
        self.id = data["id"] as! Int
        self.state = getStringFromJSON(data, key:"state")
        self.subscription = data["subscription"] as! Int
    }
    
    
    func getStringFromJSON(data: NSDictionary, key: String) -> String {
        
        if let info = data[key] as? String {
            return info
        }
        else{
            return "getStringFromJson did not werk"
        }
    }
    
}