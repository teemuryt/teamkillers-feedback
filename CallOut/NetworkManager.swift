//
//  NetworkManager.swift
//  CallOut
//
//  Created by Julius Niiniranta on 04/05/16.
//  Copyright © 2016 FeedBack TK. All rights reserved.
//

import UIKit

protocol NetworkManagerDelegate: class {
    func didFailToCompleteRequest(errorDescription: String)
    func didCompleteRequest(objectModel: NetworkManager.ObjectModel, receivedResponse: NSHTTPURLResponse, receivedDataObject: Dictionary<String, AnyObject>?, receivedDataArray: Array<Dictionary<String, AnyObject>>?)
}

class NetworkManager: NSObject, NSURLSessionDelegate {
    
    enum ObjectModel: String {
        case Users = "users"
        case Beacons = "beacons"
        case Workspaces = "workspaces"
        case Subscriptions = "subscriptions"
        case Statuses = "statuses"
    }
    
    enum RequestMethod: String {
        case GET = "GET"
        case POST = "POST"
        case PUT = "PUT"
        case PATCH = "PATCH"
        case DELETE = "DELETE"
    }
    
    override init() {
        super.init()
    }
    
    var delegate: NetworkManagerDelegate?
    var objectModel: ObjectModel?
    let apiBaseUrl = "http://1103.co:8000/callout/"
    
    func httpRequest(requestMethod: RequestMethod, objectModel: ObjectModel, individual: Int?, bodyParams: Dictionary<String, AnyObject>?) {
        self.objectModel = objectModel
        let sessionConfig = NSURLSessionConfiguration.defaultSessionConfiguration()
        let session = NSURLSession(configuration: sessionConfig)
        var requestUrl = self.apiBaseUrl
        requestUrl += self.objectModel!.rawValue
        if let requestIndividual = individual {
            requestUrl += "/\(String(requestIndividual))"
        }
        requestUrl += ".json"
        let url = NSURL(string: requestUrl)
        let request = NSMutableURLRequest(URL: url!, cachePolicy: NSURLRequestCachePolicy.ReloadIgnoringCacheData, timeoutInterval: 10)
        request.HTTPMethod = requestMethod.rawValue
        
        print("NetworkManager:", "Convert body parameters")
        
        if let params = bodyParams {
            if let convertedParams: NSData = self.convertJSON(params) {
                print("NetworkManager:", "convertedParams: ", NSString(data: convertedParams, encoding: NSUTF8StringEncoding)!)
                request.HTTPBody = convertedParams
                request.addValue("application/json", forHTTPHeaderField: "Content-Type")
                request.addValue("application/json", forHTTPHeaderField: "Accept")
            } else {
                print("NetworkManager:", "Could not convert HTTP body parameters.")
            }
        } else {
            request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        }
        
        print("NetworkManager:", "requesturl:", requestUrl, "requestMethod:", requestMethod, "individual:", individual, "bodyParams:", bodyParams)
        
        let dataTask = session.dataTaskWithRequest(request, completionHandler: {
            (data: NSData?, response: NSURLResponse?, error: NSError?) in
            
            guard error == nil else {
                self.delegate?.didFailToCompleteRequest(error!.localizedDescription)
                return
            }
            
            var receivedDataArray: Array<Dictionary<String, AnyObject>>?
            var receivedDataObject: Dictionary<String, AnyObject>?
            
            if let receivedData = self.convertData(data!) {
                receivedDataArray = (receivedData as? Array) as Array?
                receivedDataObject = (receivedData as? Dictionary) as Dictionary?
                if receivedDataArray != nil {
                    print("NetworkManager:", "receivedDataArray:", receivedDataArray)
                }
                if receivedDataObject != nil {
                    if objectModel == .Beacons {
                        NSUserDefaults.standardUserDefaults().setInteger((receivedDataObject!["id"] as? Int)!, forKey: "tempBeaconId")
                    } else if objectModel == .Users {
                        NSUserDefaults.standardUserDefaults().setValue(receivedDataObject!["id"], forKey: "userId")
                        NSUserDefaults.standardUserDefaults().setValue(receivedDataObject!["name"], forKey: "workspaceName")
                    }
                    print("NetworkManager:", "receivedDataObject:", receivedDataObject)
                }
            }
            
            guard let receivedResponse = response as? NSHTTPURLResponse else {
                self.delegate?.didFailToCompleteRequest("Could not receive response from the server.")
                return
            }
            
            self.delegate?.didCompleteRequest(objectModel, receivedResponse: receivedResponse, receivedDataObject: receivedDataObject, receivedDataArray: receivedDataArray)
            
        })
        
        dataTask.resume()
    }
    
    func convertJSON(data: Dictionary<String, AnyObject>)-> NSData? {
        print("NetworkManager:", "convertJSON")
        if NSJSONSerialization.isValidJSONObject(data) {
            do {
                print("NetworkManager", "return dataWithJSONObject")
                return try NSJSONSerialization.dataWithJSONObject(data, options: .PrettyPrinted)
            } catch {
                print("NetworkManager:", "convertJSON: Error parsing JSON")
            }
        } else {
            print("NetworkManager:", "convertJSON: Invalid JSON object")
        }
        return nil
    }
    
    func convertData(data: NSData)-> AnyObject? {
        print("NetworkManager:", "convertData")
        do {
            print("NetworkManager", "return JSONObjectWithData")
            return try NSJSONSerialization.JSONObjectWithData(data, options: .AllowFragments)
        } catch let error as NSError {
            print ("NetworkManager:", "convertData:", error.localizedDescription)
        }
        return nil
    }
}
