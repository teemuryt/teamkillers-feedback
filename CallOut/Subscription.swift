//
//  Subscription.swift
//  CallOut
//
//  Created by iosdev on 27.4.2016.
//  Copyright © 2016 FeedBack TK. All rights reserved.
//

import Foundation

class Subscription: NSObject {
    
    var id: Int!
    var user: User!
    var workspace: Workspace!
    var role: Int!
    var feedback: String!
    var currentStatus: Status!
    
    init(data: NSDictionary){
        super.init()
        
        self.id = data["id"] as! Int
        self.user = getUserFromJSON(data, key: "user")
        self.workspace = getWorkspaceFromJSON(data, key: "workspace")
        self.role = data["role"] as! Int
        self.feedback = getStringFromJSON(data, key: "feedback")
        self.currentStatus = getStatusFromJSON(data, key: "currentstatus")
        
    }
    func getStringFromJSON(data: NSDictionary, key: String) -> String {
        
        if let info = data[key] as? String {
            return info
        }
        else{
            return "getStringFromJson did not werk"
        }
    }
    func getStatusFromJSON(data: NSDictionary, key: String) -> Status {
        if let info = data[key] as? NSDictionary {
            return Status(data: info)
        }
        else{
            return currentStatus
        }
    }
    
    // FUNCTIONS FOR GETTING ARRAYS THAT INCLUDE OBJECTS AND NOT IDS OF THEM
    func getUserFromJSON(data: NSDictionary, key: String) -> User {
        
        if let info = data[key] as? NSDictionary {
            return User(data: info)
        }
        else{
            return user
        }
    }
    func getWorkspaceFromJSON(data: NSDictionary, key: String) -> Workspace {
        
        if let info = data[key] as? NSDictionary {
            return Workspace(data: info)
        }
        else{
            return workspace
        }
    }
    
}