//
//  ChartsViewController.swift
//  CallOut
//
//  Created by Julius Niiniranta on 29/04/16.
//  Copyright © 2016 FeedBack TK. All rights reserved.
//

import UIKit
import Charts

class ChartsViewController: UIViewController, ChartViewDelegate{
    
    @IBOutlet weak var pieChartView: PieChartView!
    let api = callOutApiSingleton
    let statusDetector = StatusManager.sharedInstance
    var refreshControl: UIRefreshControl!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.let statuses = ["Following", "Unsure", "Requesting Assistance"]
    }
    
    override func viewDidAppear(animated: Bool) {
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setChartData(dataPoints: [String], values: [Double]) {
        for status in api.statuses {
            print("statuslista " + status.state)
            
        }
        if api.defaults.objectForKey("workspaceName") != nil{
            pieChartView.descriptionText = "Statistics on Workspace: " + String(api.defaults.objectForKey("workspaceName")!)
            pieChartView.noDataTextDescription = "No Data"
        }
        var dataEntries: [ChartDataEntry] = []
        for i in 0..<dataPoints.count {
            print("valuet " + "\(values[i])")
            let dataEntry = ChartDataEntry(value: values[i], xIndex: i)
            dataEntries.append(dataEntry)
        }
        
        let pieChartDataSet = PieChartDataSet(yVals: dataEntries, label: "")
        let pieChartData = PieChartData(xVals: dataPoints, dataSet: pieChartDataSet)
        
        pieChartDataSet.colors = [UIColor(red: 51/255, green: 204/255, blue: 51/255, alpha: 1),
                                  UIColor(red: 255/255, green: 204/255, blue: 0/255, alpha: 1),
                                  UIColor(red: 255/255, green: 51/255, blue: 0/255, alpha: 1)]
        
        pieChartView.data = pieChartData
        
    }
    override func viewWillAppear(animated: Bool) {
        statusDetector.networkManager.httpRequest(NetworkManager.RequestMethod.GET, objectModel: NetworkManager.ObjectModel.Subscriptions, individual: nil, bodyParams: nil)
        let currentStates = [Double(statusDetector.statusArrayF.count), Double(statusDetector.statusArrayU.count), Double(statusDetector.statusArrayR.count)]
        let statuses = ["Following", "Unsure", "Requesting Assistance"]
        setChartData(statuses, values: currentStates)
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}