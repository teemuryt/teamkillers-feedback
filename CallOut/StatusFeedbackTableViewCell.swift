//
//  StatusFeedbackTableViewCell.swift
//  CallOut
//
//  Created by iosdev on 20.5.2016.
//  Copyright © 2016 FeedBack TK. All rights reserved.
//

import UIKit

class StatusFeedbackTableViewCell: UITableViewCell {

    @IBOutlet weak var sendFeedback: UIButton!
    @IBOutlet weak var statusFeedback: UITextField!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
