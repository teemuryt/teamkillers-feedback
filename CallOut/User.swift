//
//  User.swift
//  CallOut
//
//  Created by Topi Penttilä on 21/04/16.
//  Copyright © 2016 FeedBack TK. All rights reserved.
//

import Foundation

class User: NSObject {
    
    var id : Int!
    var firstName : String!
    var lastName : String!
    var emailAddress : String!
    
    init(data : NSDictionary) {
        super.init()
        self.id = data["id"] as! Int
        self.firstName = getStringFromJSON(data, key:"first_name")
        self.lastName = getStringFromJSON(data, key:"last_name")
        self.emailAddress = getStringFromJSON(data, key:"email_address")
        
    }
    func getStringFromJSON(data: NSDictionary, key: String) -> String {
        
        if let info = data[key] as? String {
            return info
        }
        else{
            return "getStringFromJson did not werk"
        }
    }
}
