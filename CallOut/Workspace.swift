//
//  Workspaces.swift
//  CallOut
//
//  Created by iosdev on 25.4.2016.
//  Copyright © 2016 FeedBack TK. All rights reserved.
//

import Foundation

class Workspace: NSObject {
    var name: String!
    var endDate: String!
    var startDate: String!
    var id: Int!
    var beacons: Array<Int> = []
    var admins: Array<User> = []
    var attendees: Array<User> = []
    
    init(data : NSDictionary) {
        
        super.init()
        self.id = data["id"] as! Int
        self.name = getStringFromJSON(data, key:"name")
        self.endDate = getStringFromJSON(data, key:"date_ending")
        self.startDate = getStringFromJSON(data, key:"date_starting")
        self.beacons = getArrayFromJSON(data, key: "beacons")
        self.admins = getAdminsFromJSON(data, key: "admins")
        self.attendees = getAttendesFromJSON(data, key: "attendees")
    }
    
    func getStringFromJSON(data: NSDictionary, key: String) -> String {
        
        if let info = data[key] as? String {
            return info
        }
        return "getStringFromJSON function failed"
    }
    func getArrayFromJSON(data: NSDictionary, key: String) -> Array<Int> {
        if let arrayInfo:Array<Int> = data[key] as? Array{
            return arrayInfo
        }
        let errorArray: Array<Int> = []
        return errorArray
    }
    
    func getAdminsFromJSON(data: NSDictionary, key: String) -> Array<User> {
        var admins: Array<User> = []
        if let arrayInfo:Array<AnyObject> = data[key] as? Array{
            for user in arrayInfo{
                let user = User(data: user as! NSDictionary)
                admins.append(user)
                print(user)
            }
        }
        return admins
    }
    
    func getAttendesFromJSON(data: NSDictionary, key: String) -> Array<User> {
        var attendees: Array<User> = []
        if let arrayInfo:Array<AnyObject> = data[key] as? Array{
            for user in arrayInfo{
                let user = User(data: user as! NSDictionary)
                attendees.append(user)
                print(user)
            }
        }
        return attendees
    }
}