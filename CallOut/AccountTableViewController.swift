//
//  AccountTableViewController.swift
//  CallOut
//
//  Created by Topi Penttilä on 19/04/16.
//  Copyright © 2016 FeedBack TK. All rights reserved.
//

import UIKit

private var defaultsContext = 0

class AccountTableViewController: UITableViewController, UINavigationControllerDelegate, AccountManagerDelegate {
    
    let nwm = NetworkManager()
    
    let defaults = NSUserDefaults.standardUserDefaults()
    
    var tabBarItemWorkspaces: UITabBarItem = UITabBarItem()
    var tabBarItemStatus: UITabBarItem = UITabBarItem()
    
    //MARK: Properties
    
    @IBOutlet weak var firstName: UITextField!
    @IBOutlet weak var lastName: UITextField!
    @IBOutlet weak var eMail: UITextField!
    @IBOutlet weak var loginIndicator: UIActivityIndicatorView!
    
    override func viewWillAppear(animated: Bool) {
        self.navigationItem.setHidesBackButton(true, animated:true);
        
        let tabBarControllerItems = self.tabBarController?.tabBar.items
        
        //Disable tab bar items
        if let arrayOfTabBarItems = tabBarControllerItems as! AnyObject as? NSArray{
            
            tabBarItemWorkspaces = arrayOfTabBarItems[1] as! UITabBarItem
            tabBarItemWorkspaces.enabled = false
            
            tabBarItemStatus = arrayOfTabBarItems[2] as! UITabBarItem
            tabBarItemStatus.enabled = false
            
        }
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(AccountTableViewController.observeUserDefaults), name: NSUserDefaultsDidChangeNotification, object: nil)
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(animated: Bool) {
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }
    
    //MARK: Actions
    
    @IBAction func setInfo(sender: UIBarButtonItem) {
        
        firstName.resignFirstResponder()
        lastName.resignFirstResponder()
        eMail.resignFirstResponder()
        
        
        AccountManager.sharedInstance.networkManager.httpRequest(
            .POST,
            objectModel: .Users,
            individual: nil,
            bodyParams: [
            "first_name" : self.firstName.text!,
            "last_name" : self.lastName.text!,
            "email_address" : self.eMail.text!
            ] as Dictionary<String, AnyObject>)
        
        defaults.setValue(self.firstName.text! + " " + self.lastName.text!, forKey: "username")
        navigationController?.popViewControllerAnimated(false)
    }
    
    override func viewDidAppear(animated: Bool) {
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: Observers
    
    func observeUserDefaults() {
        // When UserDefaults change, this callback method is evoked. It verifies that userId has a value, and then segues back to the User View.
        if (NSUserDefaults.standardUserDefaults().objectForKey("userId") != nil) {
            dispatch_async(dispatch_get_main_queue(), {
                self.navigationController?.popViewControllerAnimated(true)
                })
        }
        self.loginIndicator.stopAnimating()
    }
    
    func didGetUserData(userData: NSDictionary) {
        print("AccountTableViewController.didGetUserData()")
    }
    
    func didCreateNewUser(userData: NSDictionary) {
        print("AccountTableViewController.didCreateNewUser()")
    }
    
    func didModifyUser(userData: NSDictionary) {
        print("AccountTableViewController.didModifyUser()")
    }

    func didDeleteUser(userData: NSDictionary) {
        print("AccountTableViewController.didDeleteUser()")
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    

    /*
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("reuseIdentifier", forIndexPath: indexPath)

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
