
import UIKit
import CoreData

class WorkspaceTableViewController: UITableViewController{
    
    
    
    let api = callOutApiSingleton
    let detector = detectorSingleton
    let workspaceDetector = workspaceManagerSingleton
    let my = "My Workspaces:"
    let available = "Available Workspaces:"
    let nope = "workspace you havn't joined"
    let yessus = "workspace you have joined"
    
    func didReceiveError() {
    }
    func didReceiveResponse() {
        self.tableView.reloadData()
    }
    
    
    func refresh(sender:AnyObject)
    {
        print("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!TRYING TO REFRESH!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
        // Updating your data here...
        viewWillAppear(true)
        viewDidAppear(true)
        self.tableView.reloadData()
        self.refreshControl?.endRefreshing()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("VIEW DID LOAD")
        
        
        
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(animated: Bool) {
        print("VIEW WILL APPEAR NOW")
        self.tableView.reloadData()
        
        self.refreshControl?.addTarget(self, action: #selector(WorkspaceTableViewController.refresh(_:)), forControlEvents: UIControlEvents.ValueChanged)
        
        //End operation queue
        
        print("VIEW WILL APPEAR ENDING")
    }

    
    override func viewDidAppear(animated: Bool) {
        print("VIEW DID APPEAR")
        self.detector.rangeBeacons()
        //self.workspaceDetector.testiAvaWorkspaces()
        if workspaceDetector.myWorkspaces.count > 0 {
            navigationItem.leftBarButtonItem = editButtonItem()
        }
    }
    
    override func viewWillDisappear(animated: Bool) {
    }
    
    // MARK: - Table view data source
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 2
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        
        if section == 0{
            return workspaceDetector.myWorkspaces.count
        }
        else{
            return workspaceDetector.avaWorkspaces.count
        }
    }
    
    
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("workspaceTableViewCell", forIndexPath: indexPath) as! WorkspaceTableViewCell
        
        // Configure the cell...
        if indexPath.section == 0 {
            if workspaceDetector.myWorkspaces.count > indexPath.item{
                cell.workspaceTableViewCell.text = workspaceDetector.myWorkspaces[indexPath.item].name
                //cell.workspaceTableViewCell.text = workspaceDetector.myWorkspaces[indexPath.item].name  RIKKINÄINEN METODI JOS TYHJIÄ
            }
            else{
                cell.workspaceTableViewCell.text = "No workspaces, pull to refresh."
            }
            return cell
        } else {
            if workspaceDetector.avaWorkspaces.count > indexPath.item{
                cell.workspaceTableViewCell.text = workspaceDetector.avaWorkspaces[indexPath.item].name
                print("!!!!!!!!!!!!!!!!!!!!!!RELOADING CELL!!!!!!!!!!!!!!!!!!!" )
            }
            else {
                cell.workspaceTableViewCell.text = "No available workspaces, pull to refresh."
            }
            return cell
        }
    }
    override func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 0 {
            return my
        }
        else {
            return available
        }
    }
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 40
    }
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        //Alert view
        
        if indexPath.section == 0 && indexPath.item < workspaceDetector.myWorkspaces.count {
            let alert = UIAlertController(title: "Current workspace", message: "Are you sure you want to select " + workspaceDetector.myWorkspaces[indexPath.item].name + " as your active workspace?", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { action in
                switch action.style{
                case .Default:
                    if self.api.workspaces.contains(self.workspaceDetector.myWorkspaces[indexPath.item]){
                        self.api.defaults.setInteger(self.workspaceDetector.myWorkspaces[indexPath.item].id, forKey: "workspaceId")
                        self.api.defaults.setValue(self.workspaceDetector.myWorkspaces[indexPath.item].name, forKey: "workspaceName")
                        print(String(self.api.defaults.valueForKey("workspaceName")))
                    }
                case .Cancel:
                    print("Cancelled")
                    
                case .Destructive:
                    print("Destructive")
                }
            }))
            alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Default, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)
        }
        else if indexPath.item < workspaceDetector.avaWorkspaces.count{
            //YMPÄRÖI IFILLÄ JOKA CHECKKAA ETTEI INDEXPATH OLE OUT OF RANGE
            let alert = UIAlertController(title: "Join Workspace", message: "Are you sure you want to join workspace: " + workspaceDetector.avaWorkspaces[indexPath.item].name + "?", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { action in
                switch action.style{
                case .Default:
                    self.workspaceDetector.nwm.httpRequest( NetworkManager.RequestMethod.POST, objectModel: NetworkManager.ObjectModel.Subscriptions, individual: nil, bodyParams: [
                        "user": self.api.defaults.integerForKey("userId"),
                        "workspace": self.workspaceDetector.avaWorkspaces[indexPath.item].id,
                        "role": 1
                        ]as Dictionary<String, AnyObject>)
                    self.api.postSubscription(self.api.defaults.integerForKey("userId"), workspace: self.workspaceDetector.avaWorkspaces[indexPath.item].id, role: 1, feedback: "")
                    self.api.defaults.setInteger(self.workspaceDetector.avaWorkspaces[indexPath.item].id, forKey: "workspaceId")
                    self.workspaceDetector.avaWorkspaces.removeAtIndex(indexPath.item)
                case .Cancel:
                    print("cancel")
                    
                case .Destructive:
                    print("destructive")
                }
            }))
            alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Default, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)
        }
    }
    
    /* override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
     api.defaults.setInteger(workspaceDetector.avaWorkspaces[indexPath.item].id, forKey: "workspaceId")
     api.defaults.synchronize()
     print("workspace id saved " + "\(api.defaults.integerForKey("workspaceId"))")
     
     // create the alert
     let alert = UIAlertController(title: "Join Workspace", message: "Joining workspace" + "\(workspaceDetector.avaWorkspaces[indexPath.item].id)", preferredStyle: UIAlertControllerStyle.Alert)
     
     // add the actions (buttons)
     alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: {action in
     self.workspaceDetector.joinWorkspace(self.workspaceDetector.avaWorkspaces[indexPath.item])
     }))
     
     alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel, handler: nil))
     
     // show the alert
     self.presentViewController(alert, animated: true, completion: nil)
     
     }*/
    
    
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        if indexPath.section == 0 {
            return true
            
        }else {
            return false
        }
    }
    
    
    
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        
        if editingStyle == .Delete {
            print("deleted " + "\(workspaceDetector.myWorkspaces[indexPath.row].name)")
            api.deleteWorkspace(workspaceDetector.myWorkspaces[indexPath.row].id)
            workspaceDetector.myWorkspaces.removeAtIndex(indexPath.row)
            print(api.workspaces[indexPath.row])
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
            
            // Delete the row from the data source
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }
    }
    
    
    /*
     // Override to support rearranging the table view.
     override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {
     
     }
     */
    
    /*
     // Override to support conditional rearranging of the table view.
     override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
     // Return false if you do not want the item to be re-orderable.
     return true
     }
     */
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}