//
//  CreateWorkspaceTableViewController.swift
//  CallOut
//
//  Created by iosdev on 22.4.2016.
//  Copyright © 2016 FeedBack TK. All rights reserved.
//

import UIKit

class CreateWorkspaceTableViewController: UITableViewController, UITextFieldDelegate, BeaconDetectorDelegate {
    
    let defaults = NSUserDefaults.standardUserDefaults()
    
    func didFinishTask() {
        tableView.reloadData()
    }
    
    
    @IBOutlet var createWorkspaceTableView: UITableView!
    
    let api = callOutApiSingleton
    let workspaceManager = workspaceManagerSingleton
    
    var tempWorkspaceName: String = ""
    
    var testiStringi: String = ""
    var tempDate: NSDate!
    var joku: String = ""
    var textlolField: UITextField?
    var name: String = ""
    
    //MARK: Properties
    
    
    let detector = detectorSingleton
    
    func refresh(sender:AnyObject)
    {
        // Updating your data here...
        
        self.tableView.reloadData()
        self.refreshControl?.endRefreshing()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // self.workSpaceName.delegate = self;
        self.refreshControl?.addTarget(self, action: #selector(CreateWorkspaceTableViewController.refresh(_:)), forControlEvents: UIControlEvents.ValueChanged)
        
        
        // Do any additional setup after loading the view.
    }
    
    
    override func viewWillAppear(animated: Bool) {
        detector.rangeBeacons()
        workspaceManager.nwm.httpRequest(.GET, objectModel: .Beacons, individual: nil, bodyParams: nil)
    }
    
    override func viewDidAppear(animated: Bool) {
        api.getAll()
    }
    
    override func viewWillDisappear(animated: Bool) {
        detector.stopRangingBeacons()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func textFieldDidBeginEditing(textField: UITextField) {
        textlolField = textField
    }
    
    func textFieldDidEndEditing(textlolField: UITextField) {
        name = textlolField.text!
        
    }
    
    
    //MARK: Actions
    
    @IBAction func workspaceDone(sender: UIBarButtonItem) {
        let queue = NSOperationQueue()
        queue.maxConcurrentOperationCount=1
        textlolField?.resignFirstResponder()
        
        let postWorkspaceOperation = NSBlockOperation(block: {
            print(self.textlolField!.text!)
            print(self.defaults.valueForKey("username"))
            self.workspaceManager.nwm.httpRequest(.POST, objectModel: .Workspaces, individual: nil,             bodyParams: [
                "name" : String(self.textlolField!.text!),
                "creator" : Int(self.defaults.integerForKey("userId")),
                "beacons" : Int(self.defaults.integerForKey("tempBeaconId"))
                ] as Dictionary<String, AnyObject>)
        })
        queue.addOperation(postWorkspaceOperation)
        let alert = UIAlertController(title: "Workspace created", message: "Workspace  " + String(defaults.valueForKey("workspaceName")) + " created", preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil))
        self.presentViewController(alert, animated: true, completion: nil)
        
        
        /*nameSave.text = workSpaceName.text
         
         let dateFormatter = NSDateFormatter()
         
         dateFormatter.dateStyle = NSDateFormatterStyle.ShortStyle
         dateFormatter.timeStyle = NSDateFormatterStyle.ShortStyle
         let strDate = dateFormatter.stringFromDate(datePicker.date)
         dateSave.text = strDate*/
        
    }
    func textFieldShouldReturn(workspaceName: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
    // MARK: - Table view data source
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 2
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        if section == 0{
            return 1
        }
        else{
            return detector.beaconAmount()
        }
    }
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if indexPath.section == 0{
            return 245
        }
        else{
            return 50
        }
    }
    override func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 0{
            return "Fill in all information:"
        }
        else {
            return "Available beacons:"
        }
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        if indexPath.section == 0{
            let cellone = tableView.dequeueReusableCellWithIdentifier("workspaceName", forIndexPath: indexPath) as! CreateWorkspaceTableViewCell
            testiStringi = cellone.workspaceName.text!
            
            cellone.workspaceName.delegate = self
            
            // let newWorkspace = Workspace
            // cell.workspaceName = newWorkspace.setValue
            // cell.nameSave = nameSave
            // cell.dateSave = dateSave
            // cell.datePicker = datePicker
            return cellone
        } else {
            let celltwo = tableView.dequeueReusableCellWithIdentifier("workspaceBeacon", forIndexPath: indexPath) as! CreateWorkspaceBeaconTableViewCell
            if !detector.maxBeacons.isEmpty {
                celltwo.workspaceBeacon.text = "Beacon major: " + String(detector.maxBeacons[indexPath.item].major) + " Minor: " + String(detector.maxBeacons[indexPath.item].minor)
            }
            return celltwo
        }
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if indexPath.section == 1 {
            
            
            //Post beacon if it's not in teh db
            var postBeacon = true
            for beacon in workspaceManager.beacons {
                if  String(beacon["major"]!) == String(detector.maxBeacons[indexPath.row].major) && String(beacon["minor"]!) == String(detector.maxBeacons[indexPath.row].minor) {

                    postBeacon = false
                    NSUserDefaults.standardUserDefaults().setInteger((beacon["id"] as? Int)!, forKey: "tempBeaconId")
                    break
                }
            }
            if postBeacon {
                workspaceManager.nwm.httpRequest(.POST, objectModel: .Beacons, individual: nil, bodyParams: [
                    "major" : String(detector.maxBeacons[indexPath.row].major),
                    "minor" : String(detector.maxBeacons[indexPath.row].minor),
                    "uuid" : String(detector.maxBeacons[indexPath.row].proximityUUID)
                    ] as Dictionary<String, AnyObject>)
            }
            
        }
    }

    
    
    
    
    
    
    /*
     override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
     let cell = tableView.dequeueReusableCellWithIdentifier("reuseIdentifier", forIndexPath: indexPath)
     
     // Configure the cell...
     
     return cell
     }
     */
    
    /*
     // Override to support conditional editing of the table view.
     override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
     // Return false if you do not want the specified item to be editable.
     return true
     }
     */
    
    /*
     // Override to support editing the table view.
     override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
     if editingStyle == .Delete {
     // Delete the row from the data source
     tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
     } else if editingStyle == .Insert {
     // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
     }
     }
     */
    
    /*
     // Override to support rearranging the table view.
     override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {
     
     }
     */
    
    /*
     // Override to support conditional rearranging of the table view.
     override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
     // Return false if you do not want the item to be re-orderable.
     return true
     }
     */
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */

}

