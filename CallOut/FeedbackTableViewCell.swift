//
//  FeedbackTableViewCell.swift
//  CallOut
//
//  Created by iosdev on 20.5.2016.
//  Copyright © 2016 FeedBack TK. All rights reserved.
//

import UIKit

class FeedbackTableViewCell: UITableViewCell {
    @IBOutlet weak var feedbackText: UITextView!

    @IBOutlet weak var feedbackLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
