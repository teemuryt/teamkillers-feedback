//
//  WorkspaceTableViewCell.swift
//  CallOut
//
//  Created by Topi Penttilä on 15/04/16.
//  Copyright © 2016 FeedBack TK. All rights reserved.
//

import UIKit

class WorkspaceTableViewCell: UITableViewCell {

    //MARK: Properties
    @IBOutlet weak var workspaceTableViewCell: UILabel!

    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    

}
