//
//  CreateWorkspaceTableViewCell.swift
//  CallOut
//
//  Created by iosdev on 22.4.2016.
//  Copyright © 2016 FeedBack TK. All rights reserved.
//

import UIKit

class CreateWorkspaceTableViewCell: UITableViewCell {
    //:MARK PROPERTIES
    
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var workspaceName: UITextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
