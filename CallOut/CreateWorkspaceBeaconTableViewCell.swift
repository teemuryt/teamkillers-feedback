//
//  CreateWorkspaceBeaconTableViewCell.swift
//  CallOut
//
//  Created by iosdev on 25.4.2016.
//  Copyright © 2016 FeedBack TK. All rights reserved.
//

import UIKit

class CreateWorkspaceBeaconTableViewCell: UITableViewCell {
    @IBOutlet weak var workspaceBeacon: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
