//
//  AdminStatusTableViewController.swift
//  CallOut
//
//  Created by iosdev on 26.4.2016.
//  Copyright © 2016 FeedBack TK. All rights reserved.
//

import UIKit
import Charts

class AdminStatusTableViewController: UITableViewController, ChartViewDelegate {
    
    let api = callOutApiSingleton
    let statusDetector = StatusManager.sharedInstance
    
    func refresh(sender:AnyObject){
        self.tableView.reloadData()
        self.refreshControl?.endRefreshing()
        viewWillAppear(true)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        print("VIEW DID LOAD HERE")
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }
    override func viewWillAppear(animated: Bool) {
        self.refreshControl?.addTarget(self, action: #selector(WorkspaceTableViewController.refresh(_:)), forControlEvents: UIControlEvents.ValueChanged)
        //self.statusDetector.delegate = self
        NSUserDefaults.standardUserDefaults().setInteger(1, forKey: "workspaceId")
        if let currentWorkspaceId: Int = NSUserDefaults.standardUserDefaults().integerForKey("workspaceId") {
            statusDetector.networkManager.httpRequest(.GET, objectModel: .Workspaces, individual: currentWorkspaceId, bodyParams: nil)
        }
    }
    
    override func viewDidAppear(animated: Bool) {
        print("VIEW DID APPEAR")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 40
    }

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 2
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        if section == 0 {
        return statusDetector.attendeesInArray.count
        }
         return statusDetector.adminsInArray.count
    }
    

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
            let cellone = tableView.dequeueReusableCellWithIdentifier("statusPeople", forIndexPath: indexPath) as! StatusPeopleTableViewCell
        print("cellone almost there")
        if indexPath.section == 0 && statusDetector.attendeesInArray.count > 0 {
            cellone.statusPerson.text = String(statusDetector.attendeesInArray[indexPath.item]["first_name"]!)
            print("cellone returns")
            return cellone
        }
        if indexPath.section == 1 && statusDetector.adminsInArray.count > 0{
            cellone.statusPerson.text = String(statusDetector.adminsInArray[indexPath.item]["fist_name"]!)
        }
        else{    cellone.statusPerson.text = "No users in your workspace"
            return cellone
        }
        return cellone
    }
    
    
    override func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 0{
        return "Users attending your workspace"
        }
        else{
            return "Admins attending your workspace"
        }
        
    }
    

    /*
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("reuseIdentifier", forIndexPath: indexPath)

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
