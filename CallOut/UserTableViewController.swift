//
//  UserTableViewController.swift
//  CallOut
//
//  Created by Julius Niiniranta on 02/05/16.
//  Copyright © 2016 FeedBack TK. All rights reserved.
//

import UIKit
import ChameleonFramework


class UserTableViewController: UITableViewController, UINavigationControllerDelegate {
    
    let api = callOutApiSingleton
    let nwm = NetworkManager()
    
    var tabBarItemWorkspaces: UITabBarItem = UITabBarItem()
    var tabBarItemStatus: UITabBarItem = UITabBarItem()
    
    // MARK: Outlets
    @IBOutlet weak var firstNameLabel: UILabel!
    @IBOutlet weak var lastNameLabel: UILabel!
    @IBOutlet weak var emailAddressLabel: UILabel!
    @IBOutlet weak var coreId: UILabel!
    
    let colors:[UIColor] = [
        UIColor.flatPurpleColorDark(),
        UIColor.flatWhiteColor()
    ]
    
    let defaults = NSUserDefaults.standardUserDefaults()
    
    
    override func viewWillAppear(animated: Bool) {
        print("viewill")
        
        let tabBarControllerItems = self.tabBarController?.tabBar.items
        
        //Enable tab bar items
        if let arrayOfTabBarItems = tabBarControllerItems as! AnyObject as? NSArray{
            
            tabBarItemWorkspaces = arrayOfTabBarItems[1] as! UITabBarItem
            tabBarItemWorkspaces.enabled = true
            
            tabBarItemStatus = arrayOfTabBarItems[2] as! UITabBarItem
            tabBarItemStatus.enabled = true
            
        }
        
        if defaults.valueForKey("userId") == nil {
            performSegueWithIdentifier("goToAccountView", sender: nil)
            //self.tabBarController?.tabBar.hidden = true
        }

        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(UserTableViewController.observeUserDefaults), name: NSUserDefaultsDidChangeNotification, object: nil)
        updateLabels()
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(animated: Bool) {
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let userId = defaults.integerForKey("userId")
                //Uncompleted method
                firstNameLabel.text = String(NSUserDefaults.standardUserDefaults().valueForKey("username")!)
                //lastNameLabel.text = user.lastName
                //emailAddressLabel.text = user.emailAddress
        
        navigationItem.leftBarButtonItem = editButtonItem()
    }
    
        override func viewDidAppear(animated: Bool) {
            
        }
    
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        if indexPath.row == 0 || indexPath.row == 1 || indexPath.row == 2 {
            return true
            
        }else {
            return false
        }
    }

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    
    // MARK: Actions

    @IBAction func logoutButton(sender: AnyObject) {
        defaults.setValue(nil, forKey: "userId")
        defaults.setValue(nil, forKey: "username")
        api.defaults.setValue(nil, forKey: "userId")
        api.defaults.setValue(nil, forKey: "username")
        
    }
    
    @IBAction func deleteAccountButton(sender: AnyObject) {
        if let userIdToBeDeleted: Int = NSUserDefaults.standardUserDefaults().integerForKey("userId") {
            let deleteAccountAlert = UIAlertController(title: "Delete Account", message: "Are you sure you want to permanently delete account number \(String(userIdToBeDeleted))?", preferredStyle: UIAlertControllerStyle.Alert)
            deleteAccountAlert.addAction(UIAlertAction(title: "Delete", style: .Default, handler: { action in
                switch action.style{
                case .Default:
                    AccountManager.sharedInstance.networkManager.httpRequest(.DELETE ,objectModel: .Users, individual: userIdToBeDeleted, bodyParams: nil)
                    self.defaults.setValue(nil, forKey: "userId")
                    self.defaults.setValue(nil, forKey: "username")
                    self.api.defaults.setValue(nil, forKey: "userId")
                    self.api.defaults.setValue(nil, forKey: "username")
                case .Cancel:
                    print("cancel")
                    
                case .Destructive:
                    print("destructive")
                }
            }))
            deleteAccountAlert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Default, handler: nil))
            self.presentViewController(deleteAccountAlert, animated: true, completion: nil)
        } else {
            print("User not logged in")
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 5
    }
    
    // MARK: Observers
    
    func observeUserDefaults() {
        // When UserDefaults change, this callback method is evoked. It verifies that userId has a value, and then segues back to the User View.
        if (NSUserDefaults.standardUserDefaults().objectForKey("userId") != nil) {
            dispatch_async(dispatch_get_main_queue(), {
                self.navigationController?.popViewControllerAnimated(false)
            })
        }
    }
    
    func didFailToReceiveResponse(errorDescription: String) {
        dispatch_async(dispatch_get_main_queue(), {
            let didFailToCreateRequestAlert = UIAlertController(title: "Request Failed", message: errorDescription, preferredStyle: UIAlertControllerStyle.Alert)
            didFailToCreateRequestAlert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: nil))
            self.presentViewController(didFailToCreateRequestAlert, animated: true, completion: nil)
        })
    }
    
    func didReceiveError(errorDescription: String) {
        dispatch_async(dispatch_get_main_queue(), {
            let didReceiveErrorAlert = UIAlertController(title: "Error", message: errorDescription, preferredStyle: UIAlertControllerStyle.Alert)
            didReceiveErrorAlert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: nil))
            self.presentViewController(didReceiveErrorAlert, animated: true, completion: nil)
        })
    }
    
    func didReceiveResponse(response: String) {
        print("Callback response:", response)
        
    }
    
    func didReceiveData(objectModel: String, receivedData: NSArray) {
        print("Callback data:", receivedData)
    }

    
    // MARK: Class methods
    
    func updateLabels() -> Void {
        print("updateLabels()")
    }

    /*
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("reuseIdentifier", forIndexPath: indexPath)

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
