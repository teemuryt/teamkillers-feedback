    
    //
    //  StatusManager2.swift
    //  CallOut
    //
    //  Created by Julius Niiniranta on 23/05/16.
    //  Copyright © 2016 FeedBack TK. All rights reserved.
    //
    
    import UIKit
    
    import Foundation
    
    protocol StatusManagerDelegate: class {
        func didGetAttendees(attendeesArray: Array<Dictionary<String, AnyObject>>)
        func didGetAdmins(adminsArray: Array<Dictionary<String, AnyObject>>)
        func didReceiveAlertMessage(alertMessage: String)
    }
    
    class StatusManager: NSObject, NetworkManagerDelegate {
        
        static let sharedInstance = StatusManager()
        var delegate: StatusManagerDelegate?
        let networkManager = NetworkManager()
        var adminsInArray: Array<Dictionary<String, AnyObject>> = []
        var attendeesInArray: Array<Dictionary<String, AnyObject>> = []
        var statusArrayF: Array<Dictionary<String, AnyObject>> = []
        var statusArrayR: Array<Dictionary<String, AnyObject>> = []
        var statusArrayU: Array<Dictionary<String, AnyObject>> = []
        
        private override init() {
            super.init()
            networkManager.delegate = self
        }
        
        func didCompleteRequest(objectModel: NetworkManager.ObjectModel, receivedResponse: NSHTTPURLResponse, receivedDataObject: Dictionary<String, AnyObject>?, receivedDataArray: Array<Dictionary<String, AnyObject>>?) {
            print("AdminDataManager: didCompleteRequest")
            guard let activeWorkspace: Int = NSUserDefaults.standardUserDefaults().integerForKey("workspaceId") else {
                self.delegate?.didReceiveAlertMessage("No active workspace.")
                return
            }
            print("AdminDataManager: activeWorkspace")
            if let receivedObject = receivedDataObject {
                print("AdminDataManager.receivedObject")
                switch objectModel {
                case .Workspaces:
                    
                    guard activeWorkspace == receivedObject["id"] as? Int else {
                        self.delegate?.didReceiveAlertMessage("Could not identify active workspace.")
                        return
                    }
                    print("AdminDataManager: activeWorkspace == receivedObject['id']")
                    guard let receivedAttendeesList = receivedObject["attendees"] as? Array<Dictionary<String, AnyObject>> else {
                        self.delegate?.didReceiveAlertMessage("Could not find a list for attendees.")
                        return
                    }
                    print("AdminDataManager: receivedAttendeeslist")
                    guard let receivedAdminsList = receivedObject["admins"] as? Array<Dictionary<String, AnyObject>> else {
                        self.delegate?.didReceiveAlertMessage("Could not find a list for admins.")
                        return
                    }
                    print("AdminDataManager: receivedAdminsList")
                    adminsInArray = receivedAdminsList
                    attendeesInArray = receivedAttendeesList
                    self.delegate?.didGetAttendees(receivedAttendeesList)
                    self.delegate?.didGetAdmins(receivedAdminsList)
                    
                
                default:
                    self.delegate?.didReceiveAlertMessage("Unknown response from the server.")
                }
                
                //            switch receivedResponse.statusCode {
                //            case 200:
                //                break
                //            case 201:
                //                break
                //            default:
                //                print("Unknown response status code.")
                //            }
                //        } else {
                //            switch receivedResponse.statusCode {
                //            case 400...499:
                //                print("Client error.")
                //            case 500...599:
                //                print("Server error.")
                //            default:
                //                print("Unknown response status code.")
                //            }
            }
            if let receivedArray = receivedDataArray {
                switch objectModel {
                    
                case .Subscriptions:
                    statusArrayF.removeAll()
                    statusArrayR.removeAll()
                    statusArrayU.removeAll()
                    for sub in receivedArray{
                        if sub["workspace"]!["id"] as? Int == activeWorkspace{
                            switch sub["currentstatus"]!["state"] as? String {
                            case "F"?:
                                statusArrayF.append((sub["currentstatus"] as? Dictionary<String, AnyObject>)!)
                            case "R"?:
                                statusArrayR.append((sub["currentstatus"] as? Dictionary<String, AnyObject>)!)
                            case "U"?:
                                statusArrayU.append((sub["currentstatus"] as? Dictionary<String, AnyObject>)!)
                            default:
                                print("lololsome error here")
                            }
                        }
                    }
                
                
                default:
                    self.delegate?.didReceiveAlertMessage("Unknown response from the server")
                }
            }
            
        }
        
        func didFailToCompleteRequest(errorDescription: String) {
            print("didFailToCompleteRequest", errorDescription)
        }
    }

