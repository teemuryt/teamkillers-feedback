//
//  StatusTableViewCell.swift
//  CallOut
//
//  Created by iosdev on 14.4.2016.
//  Copyright © 2016 FeedBack TK. All rights reserved.
//

import UIKit

class StatusTableViewCell: UITableViewCell {
    
    
    //MARK: Properties
    @IBOutlet weak var myLabel: UILabel!
    @IBOutlet weak var statusView: UIView!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

}
